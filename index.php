<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-top">

	<section class="p-top1">
		<div class="p-top1__mainvisual">
			<img class="pc-only" src="/assets/image/index/100.jpg" alt="" width="1920" height="800">
			<img class="sp-only" src="/assets/image/index/100-sp.jpg" alt="">
		</div>
		<nav class="c-gnavi pc-only">
			<ul>
				<li>
					<a href="">キャンペーン情報</a>
				</li>
				<li>
					<a href="">ご利用料金</a>
				</li>
				<li>
					<a href="">ご予約</a>
				</li>
				<li>
					<a href="">講師紹介</a>
				</li>
				<li>
					<a href="">店舗のご案内</a>
				</li>
			</ul>
		</nav>
	</section>
	
	<section class="p-top2">

		<div class="p-top2__title">
			<img class="p-top2__bgttl sp-only"src="/assets/image/index/102-sp.png" alt="">
			<h2>
				<img class="pc-only" src="/assets/image/index/103.png" alt="" width="300" height="110">
				<img class="sp-only" src="/assets/image/index/103-sp.png" alt="" width="150" height="55">
			</h2>
		</div>

		<div class="l-container">
			
			<ul class="c-list1">
				<li>
					<img class="pc-only" src="/assets/image/index/104.png" alt="" width="480" height="430">
					<img class="sp-only" src="/assets/image/index/104-sp.png" alt="">
				</li>
				<li>
					<img class="pc-only" src="/assets/image/index/105.png" alt="" width="480" height="430">
					<img class="sp-only" src="/assets/image/index/105-sp.png" alt="">
				</li>
				<li>
					<img class="pc-only" src="/assets/image/index/106.png" alt="" width="480" height="430">
					<img class="sp-only" src="/assets/image/index/106-sp.png" alt="">
				</li>
			</ul>

			<div class="c-btn1">
				<a href="">見学・体験レッスンのご予約はこちら</a>
			</div>
		</div>
	</section>

	<section class="p-top3">

		<div class="l-container">
			
			<div class="c-title1">
				<h2><img src="/assets/image/index/107.png" alt="" width="400" height="80"></h2>
			</div>

			<div class="c-title2">
				<h3>事前申し込み＜期間限定＞</h3>
			</div>
			<div class="p-top3__box">
				
				<div class="p-top3__col">
					
					<div class="c-item1">
						<p class="c-item1__ttl">体験レッスン</p>
						<p class="c-item1__price1">¥2,000<span>(税抜)</span></p>
						<p class="c-item1__price2"><span>¥0</span></p>
					</div>

					<div class="c-item1">
						<p class="c-item1__ttl">フリー練習</p>
						<p class="c-item1__price1">¥1,500<span>(税抜)</span></p>
						<p class="c-item1__price2"><span>¥0</span></p>
					</div>
				</div>
			</div>

			<div class="c-title2">
				<h3>通常料金 ※(税抜)</h3>
			</div>

			<div class="c-table1 pc-only">
				<table>
					<thead>
						<tr>
							<th colspan="2"></th>
							<th>会員</th>
							<th>ビジター</th>
							<th>フリー練習費</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>⼊会⾦</th>
							<td></td>
							<td>￥5,000／1回</td>
							<td>−</td>
							<td>−</td>
						</tr>
						<tr>
							<th>レギュラー</th>
							<td>全⽇利⽤可</td>
							<td>￥10,000（税抜）／⽉額</td>
							<td>−</td>
							<td class="u-color1 u-bold">無料</td>
						</tr>
						<tr>
							<th>デイタイム</th>
							<td>平⽇昼限定（11:00〜17:00）</td>
							<td>￥7,000（税抜）／⽉額</td>
							<td>−</td>
							<td>時間外有料</td>
						</tr>
						<tr>
							<th>ジュニア</th>
							<td>⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</td>
							<td>￥6,000（税抜）／⽉額</td>
							<td>−</td>
							<td>時間外有料</td>
						</tr>
						<tr>
							<th>ホリデイ</th>
							<td>⼟・⽇・祝のみ終⽇利⽤可</td>
							<td>￥8,000（税抜）／⽉額</td>
							<td>−</td>
							<td>時間外有料</td>
						</tr>
						<tr>
							<th>レッスン4</th>
							<td>レッスン4回<br>（スタンプカード制／有効期間：3ヶ月）</td>
							<td>−</td>
							<td>￥8,000（税抜）／1セット</td>
							<td>有料</td>
						</tr>
						<tr>
							<th>フリー練習</th>
							<td>空打席があれば利⽤可<br>※レッスンは含まず</td>
							<td>
								<span class="line1">￥1,000（税抜）／1回</span><br>
								<span class="u-color1">プレオープン期間限定 ￥0</span>
							</td>
							<td>
								<span class="line1">￥1,500（税抜）／1回</span><br>
								<span class="u-color1">プレオープン期間限定 ￥0</span>
							</td>
							<td>有料</td>
						</tr>
						<tr>
							<th>体験レッスン</th>
							<td>全⽇利⽤可</td>
							<td>−</td>
							<td>￥2,000（税抜）／1回</td>
							<td>−</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="c-tabsp sp-only">
				
				<div class="c-tabsp__ttl">
					<p>入会金</p>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>⼊会⾦</dt>
						<dd>￥5,000（税抜）／1回</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>レギュラー</p><span>全⽇利⽤可</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>￥10,000（税抜）／⽉額</dd>
						<dt>ビジター</dt>
						<dd>-</dd>
						<dt>フリー練習費</dt>
						<dd>無料</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>デイタイム</p><span>平⽇昼限定（11:00〜17:00）</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>￥7,000（税抜）／⽉額</dd>
						<dt>ビジター</dt>
						<dd>-</dd>
						<dt>フリー練習費</dt>
						<dd>時間外有料</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>ジュニア</p><span>⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>￥6,000（税抜）／⽉額</dd>
						<dt>ビジター</dt>
						<dd>-</dd>
						<dt>フリー練習費</dt>
						<dd>時間外有料</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>ホリデイ</p><span>⼟・⽇・祝のみ終⽇利⽤可</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>￥8,000（税抜）／⽉額</dd>
						<dt>ビジター</dt>
						<dd>-</dd>
						<dt>フリー練習費</dt>
						<dd>時間外有料</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>レッスン4</p><span>レッスン4回（スタンプカード制<br>／有効期間：3ヶ月）</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>-</dd>
						<dt>ビジター</dt>
						<dd>￥8,000（税抜）／1セット</dd>
						<dt>フリー練習費</dt>
						<dd>有料</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>フリー練習</p><span>空打席があれば利⽤可<br>※レッスンは含まず</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>
							<span class="c-tabsp__line">￥1,000（税抜）／1回</span>
							<span class="u-color1">プレオープン期間限定 ￥0</span>
						</dd>
						<dt>ビジター</dt>
						<dd>
							<span class="c-tabsp__line">￥1,500（税抜）／1回</span>
							<span class="u-color1">プレオープン期間限定 ￥0</span>
						</dd>
						<dt>フリー練習費</dt>
						<dd>有料</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>

				<div class="c-tabsp__ttl">
					<p>体験レッスン</p><span>全⽇利⽤可</span>
				</div>
				<div class="c-tabsp__table">
					<dl>
						<dt>会員</dt>
						<dd>-</dd>
						<dt>ビジター</dt>
						<dd>￥2,000（税抜）／1回</dd>
						<dt>フリー練習費</dt>
						<dd>-</dd>
						<dt>打席料・ボール代</dt>
						<dd>無料</dd>
					</dl>
				</div>
			</div>

			<ul class="p-top3__note">
				<li>
					<div class="c-note">レッスン・フリー利用可能時間</div>
					<p class="c-text1">1時間（準備時間含む、会員・ビジター問わず）</p>
				</li>
				<li>
					<div class="c-note c-note--color1">学生割引</div>
					<p class="c-text1">上記料金より50％OFF（会員カテゴリのみ適用<br clear="sp-only">※フリー練習含まず）</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="p-top4">
		
		<div class="p-top4__bg">
			<img class="pc-only" src="/assets/image/index/108.png" alt="">
			<img class="sp-only" src="/assets/image/index/108-sp.png" alt="">
		</div>

		<div class="l-container">
			
			<h2 class="p-top4__title">
				<img class="pc-only" src="/assets/image/index/109.png" alt="" width="750" height="140">
				<img class="sp-only" src="/assets/image/index/109-sp.png" alt="" width="268" height="123">
			</h2>
			<p class="c-text1">当スクールは予約制となっております。<br>お電話、または予約システムよりご予約を<br class="sp-only">お願い致します。</p>

			<div class="p-top4__col">
				
				<div class="p-top4__item">
					<div class="c-btn2">
						<a href="">
							<img class="pc-only" src="/assets/image/index/btn2.png" alt="" width="420" height="90">
							<img class="sp-only" src="/assets/image/index/btn2-sp.png" alt="" width="281" height="50">
						</a>
					</div>
				</div>

				<div class="p-top4__item">
					<div class="c-btn2">
						<a href="">
							<img class="pc-only" src="/assets/image/index/btn3.png" alt="" width="420" height="90">
							<img class="sp-only" src="/assets/image/index/btn3-sp.png" alt="" width="281" height="50">
						</a>
					</div>
					<div class="p-top4__txt">
						<p class="c-text1">平日11:00～21:00（昼休憩13:00～14:00）／<br>
						土・日・祝10:00～19:00<br>
						定休日 火曜日</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="p-top5">
		
		<div class="l-container">
			
			<div class="c-title1">
				<h2>
					<img class="pc-only" src="/assets/image/index/110.png" alt="" width="760" height="80">
					<img class="sp-only" src="/assets/image/index/110-sp.png" alt="" width="295" height="40">
				</h2>
			</div>

			<div class="c-imgtext1">
				<div class="c-imgtext1__img">
					<img src="/assets/image/index/111.jpg" alt="" width="272" height="204">
				</div>
				<div class="c-imgtext1__txt">
					<div class="c-title3">
						<h3 class="pc-only">矢島　嘉彦</h3>
						<h3 class="sp-only">講師名が入ります</h3>
					</div>

					<p class="c-text1">1,000人以上のレッスン経験があり、初心者から上級者までそれぞれの悩みや疑問を解決し指導します。</p>
					<ul class="c-list2">
						<li>公益社団法人日本プロゴルフ協会</li>
						<li>高校生からゴルフを始め、2006年にティーチングプロとして社団法人日本プロゴルフ協会の会員となる</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="p-top6">

		<div class="l-container">
			
			<div class="c-title1">
				<h2>
					<img class="pc-only" src="/assets/image/index/112.png" alt="" width="620" height="80">
					<img class="sp-only" src="/assets/image/index/112-sp.png" alt="" width="295" height="40">
				</h2>
			</div>

			<ul class="c-list3">
				<li>
					<img src="/assets/image/index/113.jpg" alt="" width="272" height="204">
				</li>
				<li>
					<img src="/assets/image/index/114.jpg" alt="" width="272" height="204">
				</li>
				<li>
					<img src="/assets/image/index/115.jpg" alt="" width="272" height="204">
				</li>
				<li>
					<img src="/assets/image/index/116.jpg" alt="" width="272" height="204">
				</li>
				<li>
					<img src="/assets/image/index/117.jpg" alt="" width="272" height="204">
				</li>
			</ul>
		</div>
	</section>

	<section class="p-top7">

		<div class="l-container">
			
			<h2 class="p-top7__title">
				<img class="pc-only" src="/assets/image/index/118.png" alt="" width="410" height="70">
				<img class="sp-only" src="/assets/image/index/118-sp.png" alt="" width="215" height="40">
			</h2>
		</div>

		<div class="p-top7__col">
			
			<div class="p-top7__left">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3226.3798913178275!2d139.40022691574828!3d36.03543348011329!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018d5210bdb3f87%3A0xe843e88a64e460b5!2z44Kk44Oz44OJ44Ki44K044Or44OV44K544Kv44O844OrQVJST1dT5p2x5p2-5bGx5bqX!5e0!3m2!1svi!2s!4v1531464122720" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="p-top7__right">

				<div class="p-top7__box">
					
					<dl>
						<dt>営業時間</dt>
						<dd>平日11:00〜21:00(昼休憩13:00〜14:00)<br>土・日・祝10:00〜19:00</dd>
						<dt>定休日</dt>
						<dd>火曜日</dd>
						<dt>レッスン・フリー<br class="pc-only">利用可能時間</dt>
						<dd>会員・ビジター問わず1時間(準備時間含む)</dd>
						<dt>TEL</dt>
						<dd>0493-23-8015</dd>
						<dt>住所</dt>
						<dd>〒355-0028<br>埼玉県東松山市箭弓町1-13-16 安福ビル1F</dd>
					</dl>

				</div>
			</div>
		</div>
	</section>

	<section class="p-top8">

		<div class="l-container">
			
			<div class="c-title1">
				<h2><img src="/assets/image/index/121.png" alt="" width="350" height="120"></h2>
			</div>

			<ul class="c-list4">
				<li>
					<a href="">2018年0月0日<br class="sp-only">　インドアゴルフスクール ARROWS 東松山店 サイト公開しました。</a>
				</li>
			</ul>
		</div>
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>