<footer class="c-footer">
	
	<div class="c-footer1">
		
		<div class="l-container">
			
			<div class="c-btn3">
				<a href="">
					<img class="pc-only" src="/assets/image/index/btn1.png" alt="" width="483" height="90">
					<img class="sp-only" src="/assets/image/index/btn1-sp.png" alt="" width="241" height="50">
				</a>
			</div>

			<div class="c-footer1__txt">
				<p>〒355-0028 埼玉県東松山市箭弓町1-13-16 安福ビル1F　TEL.0493-23-8015<br>
				営業時間：［平日］11:00〜21:00（昼休憩13:00〜14:00）／［土・日・祝］10:00〜19:00　火曜定休<br class="pc-only">
				<span class="pc-only">レッスン・フリー利用可能時間：会員・ビジター問わず1時間（準備時間含む）</span></p>
			</div>
		</div>
	</div>

	<div class="c-footer2">
		<div class="l-container">
			<p>Copyright (c) Shin Corporation All Rights Reserved.</p>
		</div>
	</div>
</footer>

<div class="c-toTop">
	<img src="/assets/image/index/icon_top.png" alt="" width="100" height="100">
</div>

<script src="/assets/js/SmoothScroll.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>