/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

/*===================================================
To top
===================================================*/
var toTop = $('.c-toTop');
$(window).scroll(function () {
  if ($(this).scrollTop() > 150) {
    toTop.fadeIn(1000);
  } else {
    toTop.fadeOut();
  }
});
toTop.click(function () {
  $('body,html').animate({scrollTop: 0}, 500);
  return false;
});

/*===================================================
List tab SP
===================================================*/
$(function(){
  $(".c-tabsp__ttl").on("click", function() {
    $(this).next().slideToggle();
    $(this).toggleClass('is-active');
  });
});